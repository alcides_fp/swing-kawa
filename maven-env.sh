if [ ! -v M2_HOME ] ; then
   export M2_HOME=$HOME/opt/maven

   if [ -d "$M2_HOME/bin" ] ; then
      export PATH=$M2_HOME/bin:$PATH
   fi
fi
