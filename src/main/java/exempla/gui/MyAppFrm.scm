;; -*- coding: utf-8; mode: Scheme -*-

(module-name (exempla gui MyAppFrm))

(define-alias MyAppFrmUI exempla.gui.MyAppFrmUI)

(import (class java.awt.event ActionEvent WindowEvent))
(import (class java.lang System))
(import (class javax.swing JOptionPane))

(define-simple-class MyAppFrm (MyAppFrmUI)
  ;; methods
  ((onMenuSalir evt::ActionEvent)
   (invoke-special MyAppFrmUI (this) 'onMenuSalir evt)
   (System:out:println "Saliendo desde Kawa")
   (dispatchEvent (WindowEvent (this) WindowEvent:WINDOW_CLOSING)))

  ((onMenuDiHola evt::ActionEvent)
   (System:out:println "DiHola desde Kawa")
   (JOptionPane:showMessageDialog (this) "¡Hola a todos desde Kawa!")
   '()))
