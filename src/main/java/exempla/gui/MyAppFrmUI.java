package exempla.gui;
import javax.swing.*;
import java.awt.event.ActionEvent;
//import java.awt.event.WindowEvent;

public class MyAppFrmUI extends JFrame{

  private static final long serialVersionUID = 1L;
  // Variables declaration - do not modify//GEN-BEGIN:variables
  protected javax.swing.JLabel jLabel1;
  protected javax.swing.JMenuBar jMenuBar1;
  protected javax.swing.JMenu mnArchivo;
  protected javax.swing.JMenu mnSaludo;
  protected javax.swing.JMenuItem opDiHola;
  protected javax.swing.JMenuItem opSalir;
  // End of variables declaration//GEN-END:variables
  public MyAppFrmUI(){
    initComponents();
  }
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    jLabel1 = new javax.swing.JLabel();
    jMenuBar1 = new javax.swing.JMenuBar();
    mnArchivo = new javax.swing.JMenu();
    opSalir = new javax.swing.JMenuItem();
    mnSaludo = new javax.swing.JMenu();
    opDiHola = new javax.swing.JMenuItem();

    setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
    setTitle("MyAppFrmUI");
    setMinimumSize(new java.awt.Dimension(640, 480));

    jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
    jLabel1.setText("Etiqueta1:");
    jLabel1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
    getContentPane().add(jLabel1, java.awt.BorderLayout.CENTER);

    mnArchivo.setText("Archivo");

    opSalir.setText("Salir");
    opSalir.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        onMenuSalir(evt);
      }
    });
    mnArchivo.add(opSalir);

    jMenuBar1.add(mnArchivo);

    mnSaludo.setText("Saludo");

    opDiHola.setText("Di Hola ...");
    opDiHola.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        onMenuDiHola(evt);
      }
    });
    mnSaludo.add(opDiHola);

    jMenuBar1.add(mnSaludo);

    setJMenuBar(jMenuBar1);
  }// </editor-fold>//GEN-END:initComponents

  protected void onMenuDiHola(ActionEvent evt) {
    // TODO add your handling code here:
  }
  protected void onMenuSalir(ActionEvent evt) {
    // TODO add your handling code here:
    // this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
    System.out.println("onMenuSalir en Java");
  }
}
