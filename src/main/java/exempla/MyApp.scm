;; -*- coding: utf-8; mode: Scheme -*-
(module-name (exempla MyApp))
(require "gui/MyAppFrm.scm")
(import (class java.awt EventQueue))

(define-simple-class MyApp (java.lang.Runnable)
  ;; members
  (main-wnd::MyAppFrm init: (MyAppFrm))
  ;; methods
  ((run)  ;Implementa interfaz Runnable
   (main-wnd:setVisible #t))
  ;;Main
  ((main args::String[] )::void allocation:'static
   (EventQueue:invokeLater(MyApp))))
